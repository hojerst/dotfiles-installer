#!/bin/sh
# installer / bootstrapper for dotfiles (installs yadm to .local/bin)

export GITREPO="${GITREPO:-https://gitlab.com/hojerst/dotfiles.git}"
export BRANCH="${BRANCH:-}"
export YADM_URL="${YADM_URL:-https://github.com/TheLocehiliosan/yadm/raw/master/yadm}"

# wrap in function for some "partial download" protection when piped directly to /bin/sh
main() {
    mkdir -p "$HOME/.local/bin"
    ( curl -fLs "$YADM_URL" 2>/dev/null || wget -qO- "$YADM_URL" ) >"$HOME/.local/bin/yadm"
    chmod 0755 "$HOME/.local/bin/yadm"
    "$HOME/.local/bin/yadm" clone ${BRANCH:+-b "$BRANCH"} --no-bootstrap "$@" "$GITREPO"
}

main "$@"
